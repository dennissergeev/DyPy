import os
import numpy as np
import numpy.ma as ma
from numpy.testing import assert_almost_equal

from dypy.small_tools import (read_shapefile, mask_polygon,
                              potential_temperature)

testdatadir = os.path.join(os.path.dirname(__file__), 'test_data')


def test_read_shapefile_single_polygon():
    shapefile = os.path.join(testdatadir, 'shapefile_single_polygon.shp')
    pnts, projections = read_shapefile(shapefile)
    test_pnts = np.array([[8.61686488, 46.08349113],
                          [8.47050174, 45.98639637],
                          [8.34168295, 46.22408627],
                          [8.48540242, 46.29762834],
                          [8.61686488, 46.08349113]])
    assert_almost_equal(pnts[0], test_pnts)
    assert projections == "+init=epsg:4326"


def test_mask_polygon():

    array = np.zeros((20, 20)) + 10.
    polygon = np.array([[5, 5],
                        [10, 5],
                        [10, 10],
                        [5, 10],
                        [5, 5]])
    lon = np.arange(0, 20, dtype=float)
    lat = np.arange(0, 20, dtype=float)
    lon, lat = np.meshgrid(lon, lat)

    clipped = mask_polygon(polygon, array, lon, lat)

    test_clip = np.zeros((20, 20))
    test_clip[5:10, 5:10] = 10
    test_clip = ma.masked_not_equal(test_clip, 10)

    assert_almost_equal(clipped, test_clip)


def test_potential_temperature():

    t = 15. + 273.15  # K
    p = 750.  # hPa
    th = potential_temperature(t, p)
    assert_almost_equal(th, 312.83, decimal=1)
