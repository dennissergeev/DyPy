"""module with wrappers for dyntools

"""
from subprocess import Popen, PIPE, TimeoutExpired


class DynToolsException(RuntimeError):
    """Raise this when a lagranto call fails"""


def run_cmd(cmd, workingdir='.', netcdf_format='CF', cmd_header=None):
    script = """
    cd '{workingdir}'
    {cmd}
    """
    if cmd_header is None:
        script = """ . /etc/profile.d/modules.sh
        module purge
        module load dyn_tools
        export NETCDF_FORMAT={netcdf_format}
        """ + script
    else:
        script = cmd_header + script

    script = script.format(workingdir=workingdir, cmd=cmd,
                           netcdf_format=netcdf_format)
    p = Popen('bash', stdout=PIPE, stderr=PIPE, stdin=PIPE,
              universal_newlines=True)
    try:
        output, error = p.communicate(input=script)
    except TimeoutExpired:
        p.kill()
        output, error = p.communicate()
    if error or ('ERROR' in output) or ('Error' in output):
        # noinspection PyTypeChecker
        raise DynToolsException(script + output + error)
    return output
