#!/usr/bin/env python
# coding: utf-8

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

__AUTHOR__ = 'Nicolas Piaget'
__AUTHOR_EMAIL__ = 'nicolas.piaget@env.ethz.ch'

readme = open('README.rst').read()  # + '\n\n' + open('CHANGELOG.rst').read()
packages = ['dypy', 'dypy.plotting', 'dypy.tools', 'dypy.lagranto']
requirements = ['Cartopy', 'Cython', 'docopt', 'docutils', 'Fiona',
                'Jinja2', 'matplotlib', 'netCDF4', 'numpy', 'pandas',
                'path.py', 'Pillow', 'scipy', 'Shapely', 'basemap']

tests_require = ['pytest']

setup(name='DyPy',
      version='0.1',
      author=__AUTHOR__,
      author_email=__AUTHOR_EMAIL__,
      maintainer=__AUTHOR__,
      maintainer_email=__AUTHOR_EMAIL__,
      description='Collection of tools for atmospheric science',
      long_description=readme,
      packages=packages,
      setup_requires=['pytest-runner>=2.0'],
      dependency_links=[
          'git+https://github.com/matplotlib/basemap.git@master#egg=basemap-1.0.0'
      ],
      install_requires=requirements,
      scripts=['bin/quickview.py'],
      tests_require=tests_require,
      extras_require={
          'docs':  ['Sphinx', 'sphinx-rtd-theme'],
          'testing': tests_require,
      },
      include_package_data=True,
      classifiers=[
          'Development Status :: 4 - Beta',
          'Programming Language :: Python :: 2.7',
          'Programming Language :: Python :: 3.4',
      ]
      )
