.. _small_tools-package:

small tools package
===================


DocStrings
----------

.. automodule:: dypy.small_tools
   :members:
