.. _intergrid-package:

intergrid package
=================

.. automodule:: dypy.intergrid
   :members:
