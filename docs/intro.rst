Introduction
=============

The aim of the DyPy package is to wrap tools used daily in the group.
The package is divided in several subpackages.

* :ref:`lagranto-package`
    Module to read and write trajectories. Load the trajectories in a numpy array

* :ref:`netcdf-package`
    Module to read and write netCDF file.
 
* :ref:`plotting-package`
    Module to create maps. Wrapper for Basemap with possibility to overlay
    trajectories.

* :ref:`small_tools-package`
    Module with several useful function and classes, for interpolation,
    thermodynamical calculations, ...

* :ref:`soundings-package`
    Module for reading Wyoming soundings online

* :ref:`constants-package`
    Module with useful constants for atmospheric sciences

* :ref:`intergrid-package`
    Module for interpolation
